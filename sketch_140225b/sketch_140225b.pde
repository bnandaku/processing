import processing.serial.*; 
int square = 5;
int count = 0;
Serial myPort;    // The serial port
PFont myFont;     // The display font
String inString;  // Input string from serial port
int lf = 10;      // ASCII linefeed 

void draw(){}

void setup(){
  
  size (640,480);
  // myFont = loadFont("ArialMS-18.vlw"); 
  //textFont(myFont, 18); 
  println(Serial.list()[2]); 
  colorMode(HSB,height,height,height);
background(0);
 // I know that the first port in the serial list on my mac 
  // is always my  Keyspan adaptor, so I open Serial.list()[0]. 
  // Open whatever port is the one you're using. 
  myPort = new Serial(this, Serial.list()[2], 9600); 
  myPort.bufferUntil(lf); 

}

void serialEvent(Serial p){
  
  inString = p.readString();
  println(inString);
  if(inString == "Button Pressed\n"){
    
     rect(20,20,square,square);
    
    square = square*2;
    if (count > 3){
    square = 10; 
    count = 0; 
    }
    count++;
  
  
  }

}
